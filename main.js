/*## Задание
​
Сверстать [макет](https://www.figma.com/file/Do0TLndoEjGwuF9Ri7UHol/The_Ham_Steo-Project?node-id=1%3A2). Подключить динамические элементы (см. ниже)
​
#### Технические требования к верстке:
- Нужно сделать верстку только под широкоформатные мониторы, с шириной экрана 1200 пикселей или более (то есть ширина контента фиксированная в пикселях). Максимальная ширина контейнера с контентом - 1160 пикселей (кроме второго блока с квадратами). 
- Фоновое изображение в шапке должно занимать всю доступную ширину экрана и не двигаться при скролле вниз.
- Адаптивная верстка под разные разрешения экрана НЕ требуется.
- Карточки в секции `Breaking news` должны быть кликабельными ссылками.
- Секция `Gallery of best images` не обязательная для выполнения.
- Верстка должна быть выполнена без использования CSS библиотек типа Bootstrap или Materialize.
​
#### Динамические элементы на странице:
- Вкладки в секции `Our services` должны переключаться при нажатии мышью. Текст и картинки для других вкладок вставить любые.
- Кнопка `Load more` в секции `Our amazing work` имитирует подгрузку с сервера новых картинок. При ее нажатии в секции снизу должно добавиться еще 12 картинок (изображения можно взять любые). После этого кнопка исчезает.
- Кнопки на вкладке `Our amazing work` являются "фильтрами продукции". Предварительно каждой из картинок нужно присвоить одну из четырех категорий, на ваше усмотрение (на макете это `Graphic design`, `Web design`, `Landing pages`, `Wordpress`). При нажатии на кнопку категории необходимо показать только те картинки, которые относятся к данной категории. `All` показывает картинки из всех категорий. Категории можно переименовать, картинки для категорий взять любые.
- Карусель на вкладке `What people say about theHam` должна быть рабочей, по клику как на иконку фотографии внизу, так и на стрелки вправо-влево. В карусели должна меняться как картинка, так и текст. Карусель обязательно должна быть с анимацией.
- Для подключения динамических элементов можно использовать любые библиотеки - как jQuery/его плагины, так и чистый Javascript код.
   
#### Не обязательные задания продвинутой сложности:
- Кнопку `Load more` в секции `Our amazing work` можно нажать два раза, каждое нажатие добавляет 12 картинок снизу. То есть максимум в этой секции может быть расположено 36 картинок. После второго нажатия кнопка исчезает.
- Сверстать также секцию `Gallery of best images`, расположить картинки внутри блока с помощью плагина [Masonry](https://masonry.desandro.com/).
- Кнопка `Load more` в секции `Gallery of best images` также должна быть рабочей и добавлять порцию новых картинок на страницу.
- При клике на каждую из кнопок `Load more` имитировать загрузку картинок с сервера. Показывать вместо кнопки или над ней две секунды CSS анимацию загрузки (можно написать самому или взять любой пример из интернета, например [здесь](https://freefrontend.com/css-loaders/) или [здесь](http://nisnom.com/preloadery-loader/)), и только после этого добавлять картинки на страницу.
- Разместить проект в интернете с помощью [Github pages](https://pages.github.com/) или [Gitlab pages](https://docs.gitlab.com/ee/user/project/pages/) (не забудьте потом добавить ссылку в резюме).
​
Для удобства все картинки с макета размещены в [архиве](./Step%20Project%20Ham%20Pictures.zip).
 */
const tabs = document.querySelectorAll('.tabs-title');
const tabContent = document.querySelectorAll('.tabs-content li');
const tabsCont = document.querySelector('.tabs-content');
const firstLi = tabsCont.children[0];
firstLi.style.backgroundColor = "white";
firstLi.style.color = "black";

tabs.forEach((tab, index) => {
  tab.addEventListener('click', () => {
    tabs.forEach(tab => tab.classList.remove('active'));
    tab.classList.add('active');
    tabContent.forEach(content => content.style.display = 'none');
    tabContent[index].style.display = 'block';
  });
});
//---------------------------------------------------------------------



const workGridElements = document.querySelectorAll('.work-grid');
const buttonShowImage = document.querySelector('.show-image');
const tabsWorkElements = document.querySelectorAll('.tabs-work li');

buttonShowImage.addEventListener("click", showImage);
function showImage() {
  workGridElements.forEach(img => {
    if (img.classList.contains('no-show')) {
      img.style.display = 'block';
      img.classList.replace('no-show', 'show');
      buttonShowImage.style.display ="none";
    }
  });
}
//=================

const tabsWork = document.querySelectorAll('.tabs-title-work');
const workGrids = document.querySelectorAll('.work-grid');

tabsWork.forEach((tab) => {
  tab.addEventListener('click', () => {
    const activeTab = document.querySelector('.tabs-title-work.active');
    activeTab.classList.remove('active');
    tab.classList.add('active');

    const activeCategory = tab.textContent.toLowerCase().replace(' ', '-');
    workGrids.forEach((grid) => {
      grid.classList.add('no-show');
      if (grid.classList.contains(activeCategory) || activeCategory === 'all') {
        grid.classList.remove('no-show');
      }
    });
  });
});
//----------------------------------------------------------------------

const ulPeopleSay = document.querySelector(".section-7 ul");
const liItemImg = Array.from(document.querySelectorAll(".section-7 li img"));
const articleItem = Array.from(
  document.querySelectorAll(".about-section-wrapper")
);
const leftArrow = document.querySelector(".left-arrow");
const rightArrow = document.querySelector(".right-arrow");

liItemImg.forEach((el, index) => {
  el.parentElement.dataset.index = index;
});
articleItem.forEach((elem, index) => {
  elem.dataset.index = index;
});

liItemImg.forEach((element) => {
  element.addEventListener("click", (event) => {
    liItemImg.forEach((el) => {
      el.parentElement.classList.remove("active-li");
    });
    event.target.parentElement.classList.add("active-li");
    articleItem.forEach((elem) => {
      elem.classList.add("about-hide");
    });
    articleItem.forEach((elem) => {
      if (elem.dataset.index === event.target.parentElement.dataset.index) {
        elem.classList.remove("about-hide");
      }
    });
  });
});

leftArrow.addEventListener("click", moveLeft);
rightArrow.addEventListener("click", moveRight);

function moveLeft() {
  let count = 0;
  liItemImg.forEach((el) => {
    if (el.parentElement.classList.contains("active-li")) {
      count = Number(el.parentElement.dataset.index);
      el.parentElement.classList.remove("active-li");
    }
  });
  liItemImg.forEach((el) => {
    if (count === 0) {
      count = liItemImg.length;
    }
    if (Number(el.parentElement.dataset.index) === count - 1) {
      el.parentElement.classList.add("active-li");
    }
  });
  articleItem.forEach((el) => {
    el.classList.add("about-hide");
  });

  articleItem.forEach((el) => {
    if (Number(el.dataset.index) === count - 1) {
      el.classList.remove("about-hide");
    }
  });
}

function moveRight() {
  let count = 0;
  liItemImg.forEach((el) => {
    if (el.parentElement.classList.contains("active-li")) {
      count = Number(el.parentElement.dataset.index);
      el.parentElement.classList.remove("active-li");
    }
  });
  liItemImg.forEach((el) => {
    if (count === liItemImg.length - 1) {
      count = -1;
    }
    if (Number(el.parentElement.dataset.index) === count + 1) {
      el.parentElement.classList.add("active-li");
    }
  });
  articleItem.forEach((el) => {
    el.classList.add("about-hide");
  });

  articleItem.forEach((el) => {
    if (Number(el.dataset.index) === count + 1) {
      el.classList.remove("about-hide");
    }
  });
}



